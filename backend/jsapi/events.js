
jQuery(document).ready(function($) {
	

	$("#add-event").click(function(event) {
		/* Act on the event */
		let eventname = $("#eventname").val();
		let eventdesc = $("#eventdesc").val();
		if(eventname != ""){
			$.ajax({
				url:'api/add-events.php',
				method:'POST',
				data:{'eventname':eventname, 'eventdesc':eventdesc},
				success:function(data){
							// console.log(data);
							if(data.status === 3){
								alert('This event is already exsist')
							}
							if(data.status === 1){

								$("#showsuccess").html('Event Added Successfully');

								setTimeout(()=>{

									$("#showsuccess").html("")

								}, 1000)

								getEvents()


							}
						}
					})	
		}
		else{
			alert('Enter Eventname');
		}
		
	});

	function getEvents(){
		$.ajax({
			url:'api/add-events.php',
			method:'GET',
					// data:{'eventname':eventname, 'eventdesc':eventdesc},
					success:function(data){
						// console.log(data);

						var html = "";
						console.log(data[0].eventname)
						var sr=0;
						for(i=0; i<data.length; i++)
						{
							sr++;
							if(data[i].status == 'YES'){
								html+= "<tr><td>"+sr+"</td><td>"+data[i].eventname+"</td><td>"+data[i].eventdesc+"</td><td><button class='delete btn btn-sm btn-danger' data-id='"+data[i].id+"' title='Delete'> <i class='fa fa-trash'></i> </button></td></tr>";
							}
							if(data[i].status == 'NO'){
								html+= "<tr><td>"+sr+"</td><td>"+data[i].eventname+"</td><td>"+data[i].eventdesc+"</td><td><button class='retrive btn btn-sm btn-success' data-id='"+data[i].id+"' data-retrive='retrive' title='Retrive'> <i class='fa fa-recycle'></i> </button></td></tr>";
							}
							
						}	

						$('#eventsbody').html(html)
						console.log(html)

					}
				})	
	}

	$('body').on('click', '.retrive', function(){
		const id = $(this).attr('data-id');
		const retrive = $(this).attr('data-retrive');
		$.ajax({
			url:'api/delete-events.php',
			method:'POST',
			data:{'id':id, 'retrive':retrive},
			success:function(data){
				if(data.status===1){
							// alert('Event Retrived Successfully')
							getEvents();
						}
					}

				})

	})
	$('body').on('click', '.delete', function(){
		const id = $(this).attr('data-id');
		$.ajax({
			url:'api/delete-events.php',
			method:'POST',
			data:{'id':id},
			success:function(data){
				if(data.status===1){
							// alert('Event Deleted Successfully')
							getEvents();
						}
					}

				})

	})

	getEvents()

});