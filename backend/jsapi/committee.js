
jQuery(document).ready(function($) {


	$("#fupForm").on('submit', function(e){
		e.preventDefault();
		$.ajax({
			type: 'POST',
			url: './api/add-committee-new.php',
			data: new FormData(this),
			dataType: 'json',
			contentType: false,
			cache: false,
			processData:false,
			beforeSend: function(){
				$('.submitBtn').attr("disabled","disabled");
				$('#fupForm').css("opacity",".5");
			},
            success: function(response){ //console.log(response);
            	$('.statusMsg').html('');
            	if(response.status == 1){
            		// getPhotos()
            		getCommittee();
            		$('#fupForm')[0].reset();
            		$('.statusMsg').html('<p class="alert alert-success">'+response.message+'</p>');
            	}else{
            		$('.statusMsg').html('<p class="alert alert-danger">'+response.message+'</p>');
            	}
            	$('#fupForm').css("opacity","");
            	$(".submitBtn").removeAttr("disabled");

            }
        });
	});

	$("#file").change(function() {
		var file = this.files[0];
		var fileType = file.type;
		var match = [ 'image/jpeg', 'image/png', 'image/jpg'];
		if(!((fileType == match[0]) || (fileType == match[1]) || (fileType == match[2]) )){
			alert('Sorry only JPG, JPEG, & PNG files are allowed to upload.');
			$("#file").val('');
			return false;
		}
	});

	
	var imageurl="http://localhost:8080/rgc/backend/uploads/";

	// $("#add-committee").click(function(event) {
	// 	/* Act on the event */
	// 	let name = $("#name").val();
	// 	let committee = $("#committee").val();
	// 	if(name != "" && committee != ""){
	// 		$.ajax({
	// 			url:'api/add-committee.php',
	// 			method:'POST',
	// 			data:{'name':name, 'committee':committee},
	// 			success:function(data){
	// 						// console.log(data);
	// 						if(data.status === 3){
	// 							alert('This event is already exsist')
	// 						}
	// 						if(data.status === 1){

	// 							$("#showsuccess").html('Committee Added Successfully');

	// 							setTimeout(()=>{

	// 								$("#showsuccess").html("")

	// 							}, 1000)

	// 							getCommittee()

	// 							$("#name").val("");$("#committee").val("")


	// 						}
	// 					}
	// 				})	
	// 	}
	// 	else{
	// 		alert('Enter All Fields');
	// 	}
	
	// });

	function getCommittee(){
		$.ajax({
			url:'api/add-committee.php',
			method:'GET',
			success:function(data){

				var html = "";
				var sr=0;
				for(i=0; i<data.length; i++)
				{
					sr++;

					html+= "<tr><td>"+sr+"</td><td>"+data[i].name+"</td><td>"+data[i].committee+"</td><td><img src='./uploads/"+data[i].image_path+"' height='50px' width='50px'/></td><td><button class='delete btn btn-sm btn-danger' data-id='"+data[i].id+"' title='Delete'> <i class='fa fa-trash'></i> </button></td></tr>";
					
				}	

				$('#committeebody').html(html)

			}
		})	
	}


	$('body').on('click', '.delete', function(){
		const id = $(this).attr('data-id');
		$.ajax({
			url:'api/delete-committee.php',
			method:'POST',
			data:{'id':id},
			success:function(data){
				if(data.status===1){
							// alert('Event Deleted Successfully')
							getCommittee();
						}
					}

				})

	})

	getCommittee()

});