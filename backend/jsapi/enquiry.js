
jQuery(document).ready(function($) {

	function getEnquiry(){
		$.ajax({
			url:'api/get-enquiry.php',
			method:'GET',
			success:function(data){

				var html = "";
				var sr=0;
				for(i=0; i<data.length; i++)
				{
					sr++;

					html+= "<tr><td>"+sr+"</td><td>"+data[i].name+"</td><td>"+data[i].email+"</td><td>"+data[i].contact+"</td><td>"+data[i].subject+"</td><td>"+data[i].message+"</td><td><button class='delete btn btn-sm btn-danger' data-id='"+data[i].id+"' title='Delete'> <i class='fa fa-trash'></i> </button></td></tr>";


				}	

				$('#enquirybody').html(html)

			}
		})	
	}


	$('body').on('click', '.delete', function(){
		const id = $(this).attr('data-id');
		$.ajax({
			url:'api/delete-enquiry.php',
			method:'POST',
			data:{'id':id},
			success:function(data){
				if(data.status===1){
							// alert('Event Deleted Successfully')
							getEnquiry();
						}
					}

				})

	})

	getEnquiry()

});
