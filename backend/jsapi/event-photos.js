
$(document).ready(function(e){
    // Submit form data via Ajax
    $("#fupForm").on('submit', function(e){
    	e.preventDefault();
    	$.ajax({
    		type: 'POST',
    		url: './api/event-photos.php',
    		data: new FormData(this),
    		dataType: 'json',
    		contentType: false,
    		cache: false,
    		processData:false,
    		beforeSend: function(){
    			$('.submitBtn').attr("disabled","disabled");
    			$('#fupForm').css("opacity",".5");
    		},
            success: function(response){ //console.log(response);
            	$('.statusMsg').html('');
            	if(response.status == 1){
            		getPhotos()
            		$('#fupForm')[0].reset();
            		$('.statusMsg').html('<p class="alert alert-success">'+response.message+'</p>');
            	}else{
            		$('.statusMsg').html('<p class="alert alert-danger">'+response.message+'</p>');
            	}
            	$('#fupForm').css("opacity","");
            	$(".submitBtn").removeAttr("disabled");

            }
        });
    });

    $("#file").change(function() {
    	var file = this.files[0];
    	var fileType = file.type;
    	var match = [ 'image/jpeg', 'image/png', 'image/jpg'];
    	if(!((fileType == match[0]) || (fileType == match[1]) || (fileType == match[2]) )){
    		alert('Sorry only JPG, JPEG, & PNG files are allowed to upload.');
    		$("#file").val('');
    		return false;
    	}
    });



    $("#add-event").click(function(event) {
    	/* Act on the event */
    	let eventname = $("#eventname").val();
    	let eventdesc = $("#eventdesc").val();
    	if(eventname != ""){
    		$.ajax({
    			url:'api/add-events.php',
    			method:'POST',
    			data:{'eventname':eventname, 'eventdesc':eventdesc},
    			success:function(data){
							// console.log(data);
							if(data.status === 3){
								alert('This event is already exsist')
							}
							if(data.status === 1){

								$("#showsuccess").html('Event Added Successfully');

								setTimeout(()=>{

									$("#showsuccess").html("")

								}, 1000)

								getEvents()


							}
						}
					})	
    	}
    	else{
    		alert('Enter Eventname');
    	}

    });

    function getEvents(){
    	$.ajax({
    		url:'api/add-events.php',
    		method:'GET',
					// data:{'eventname':eventname, 'eventdesc':eventdesc},
					success:function(data){
						// console.log(data);

						var html = "";
						html+="<option value='0'> ---Select events--- </option>"
						console.log(data[0].eventname)
						var sr=0;
						for(i=0; i<data.length; i++)
						{
							sr++;
							if(data[i].status == 'YES'){
								html+="<option value="+data[i].eventname+">"+data[i].eventname+"</option>"

							}

							
						}	

						$('#sel_event').html(html)
						console.log(html)

					}
				})	
    }
    function getPhotos(){


    	$.ajax({
    		url:'api/get-event-photos.php',
    		method:'GET',
					// data:{'eventname':eventname, 'eventdesc':eventdesc},
					success:function(data){
						console.log(data);



						var html = "";

						// console.log(data[0].eventname)
						var sr=0;
						for(i=0; i<data.length; i++)
						{
							sr++;
							if(data[i].status == 'YES'){
								html+="<tr><td>"+sr+"</td><td>"+data[i].eventname+"</td><td>"+data[i].description+"<td><img src='./uploads/"+data[i].image_path+"' height='50px' width='50px'/></td> <td><button class='btn btn-danger delete' data-id="+data[i].id+"> <i class='fa fa-trash'></i> </button></td>";

							}



						}	

						$('#photo-table').html(html)
						// console.log(html)

					}
				})	
    }

    $('body').on('click', '.retrive', function(){
    	const id = $(this).attr('data-id');
    	const retrive = $(this).attr('data-retrive');
    	$.ajax({
    		url:'api/delete-events.php',
    		method:'POST',
    		data:{'id':id, 'retrive':retrive},
    		success:function(data){
    			if(data.status===1){
							// alert('Event Retrived Successfully')
							getEvents();
						}
					}

				})

    })
    $('body').on('click', '.delete', function(){
    	const id = $(this).attr('data-id');
    	$.ajax({
    		url:'api/delete-photo.php',
    		method:'GET',
    		data:{'id':id},
    		success:function(data){
    			if(data.status===1){

    				console.log('deleted')
    				getPhotos();
    				// getEvents();
    			}
    		}

    	})

    })

    getEvents()
    getPhotos()

});