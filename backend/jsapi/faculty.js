
jQuery(document).ready(function($) {
	

	$("#fupForm").on('submit', function(e){
		e.preventDefault();
		$.ajax({
			type: 'POST',
			url: './api/add-faculty-new.php',
			data: new FormData(this),
			dataType: 'json',
			contentType: false,
			cache: false,
			processData:false,
			beforeSend: function(){
				$('.submitBtn').attr("disabled","disabled");
				$('#fupForm').css("opacity",".5");
			},
            success: function(response){ //console.log(response);
            	$('.statusMsg').html('');
            	if(response.status == 1){
            		// getPhotos()
            		getFaculty();
            		$('#fupForm')[0].reset();
            		$('.statusMsg').html('<p class="alert alert-success">'+response.message+'</p>');
            	}else{
            		$('.statusMsg').html('<p class="alert alert-danger">'+response.message+'</p>');
            	}
            	$('#fupForm').css("opacity","");
            	$(".submitBtn").removeAttr("disabled");

            }
        });
	});

	$("#file").change(function() {
		var file = this.files[0];
		var fileType = file.type;
		var match = [ 'image/jpeg', 'image/png', 'image/jpg'];
		if(!((fileType == match[0]) || (fileType == match[1]) || (fileType == match[2]) )){
			alert('Sorry only JPG, JPEG, & PNG files are allowed to upload.');
			$("#file").val('');
			return false;
		}
	});

	$("#add-faculty").click(function(event) {
		/* Act on the event */
		let name = $("#name").val();
		let designation = $("#designation").val();
		let subject = $("#subject").val();
		let type=$("#type").val();
		if(name != "" && designation != "" && subject != ""){
			$.ajax({
				url:'api/add-faculty.php',
				method:'POST',
				data:{'name':name, 'designation':designation, 'subject':subject,'type':type},
				success:function(data){

					if(data.status === 1){

						$("#showsuccess").html('Faculty Added Successfully');

						setTimeout(()=>{

							$("#showsuccess").html("")

						}, 1000)

						getFaculty()

						$("#name").val("");$("#designation").val("");$("#subject").val("");$("#type").val('0')


					}
				}
			})	
		}
		else{
			alert('Enter All Fields');
		}
		
	});

	function getFaculty(){
		$.ajax({
			url:'api/add-faculty.php',
			method:'GET',
			success:function(data){

				var html = "";
				var sr=0;
				for(i=0; i<data.length; i++)
				{
					sr++;

					html+= "<tr><td>"+sr+"</td><td>"+data[i].name+"</td><td>"+data[i].designation+"</td><td>"+data[i].subject+"</td><td>"+data[i].type+"</td><td><img src='./uploads/"+data[i].image_path+"' height='50px' width='50px'/></td><td><button class='delete btn btn-sm btn-danger' data-id='"+data[i].id+"' title='Delete'> <i class='fa fa-trash'></i> </button></td></tr>";
					
				}	

				$('#facultybody').html(html)

			}
		})	
	}


	$('body').on('click', '.delete', function(){
		const id = $(this).attr('data-id');
		$.ajax({
			url:'api/delete-faculty.php',
			method:'POST',
			data:{'id':id},
			success:function(data){
				if(data.status===1){
							// alert('Event Deleted Successfully')
							getFaculty();
						}
					}

				})

	})

	getFaculty()

});