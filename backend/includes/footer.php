	<!-- Footer -->
	<footer class="sticky-footer bg-white">
		<div class="container my-auto">
			<div class="copyright text-center my-auto">
				<span>Copyright &copy; Rajiv Gandhi College, Karmad <?php echo date('Y') ?> | Powered By:<a target="_blank" href="https://swebsolutions.in/"> S Web Solutions Pvt. Ltd.</a></span>
			</div>
		</div>
	</footer>
			<!-- End of Footer -->