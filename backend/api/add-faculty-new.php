<?php 
$uploadDir = '../uploads/'; 
$response = array( 
    'status' => 0, 
    'message' => 'Form submission failed, please try again.' 
); 

// If form is submitted 
if(isset($_POST['name']) || isset($_POST['designation']) || isset($_POST['subject'])  || isset($_POST['type']) || isset($_POST['file'])){ 
    // Get the submitted form data 
    $name = $_POST['name']; 
    $designation = $_POST['designation'];
    $subject = $_POST['subject'];
    $type = $_POST['type'];




    // Check whether submitted data is not empty 
    if(!empty($name) && !empty($designation) && !empty($subject) && !empty($type)){ 


        $uploadStatus = 1; 

            // Upload file 
        $uploadedFile = ''; 
        if(!empty($_FILES["file"]["name"])){ 

                // File path config 
            $fileName = basename($_FILES["file"]["name"]); 
            $targetFilePath = $uploadDir . $fileName; 
            $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION); 

                // Allow certain file formats 
            $allowTypes = array('pdf', 'doc', 'docx', 'jpg', 'png', 'jpeg'); 
            if(in_array($fileType, $allowTypes)){ 
                    // Upload file to the server 
                if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){ 
                    $uploadedFile = $fileName; 
                }else{ 
                    $uploadStatus = 0; 
                    $response['message'] = 'Sorry, there was an error uploading your file.'; 
                } 
            }else{ 
                $uploadStatus = 0; 
                $response['message'] = 'Sorry, only PDF, DOC, JPG, JPEG, & PNG files are allowed to upload.'; 
            } 
        } 

        if($uploadStatus == 1){ 
                // Include the database config file 
            include('../includes/connection.php');
            global $connection;
                // Insert form data in the database 
            $insert = $connection->query("INSERT INTO faculty (name, designation, subject, type, image_path) VALUES ('".$name."','".$designation."','".$subject."','".$type."','".$uploadedFile."')"); 

            if($insert){ 
                $response['status'] = 1; 
                $response['message'] = 'Form data submitted successfully!'; 
            } 
        } 
        
    }else{ 
       $response['message'] = 'Please fill all the mandatory fields (name and email).'; 
   } 
} 



// Return response 
echo json_encode($response);