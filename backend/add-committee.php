<?php
include('includes/session.php');
include('includes/head.php');
?>
</head>

<body id="page-top">

	<!-- Page Wrapper -->
	<div id="wrapper">

		<?php include('includes/sidebar.php') ?>
		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<!-- Topbar -->
				<?php include('includes/topbar.php') ?>
				<!-- End of Topbar -->

				<!-- Begin Page Content -->
				<div class="container-fluid">

					<!-- Page Heading -->
					<div class="d-sm-flex align-items-center justify-content-between mb-4">
						<h1 class="h3 mb-0 text-gray-800">Committee</h1>

					</div>


					<!-- Content Row -->

					<div class="row">

						<!-- Area Chart -->
						<div class="col-xl-12 col-lg-12">
							<div class="card shadow mb-4">
								<!-- Card Header - Dropdown -->
								<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
									<h6 class="m-0 font-weight-bold text-primary">Add Committee</h6>

								</div>
								<!-- Card Body -->
								<div class="card-body">


									<form id="fupForm" enctype="multipart/form-data">

										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													Name:

													<input type="text" name="name" id="name" class="form-control" placeholder="Enter name">

												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													Committee:
													<input type="text" name="committee" id="committee" class="form-control" placeholder="Enter Committee">

												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													Image:
													<input type="file" name="file"  class="form-control" >

												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													&nbsp;
													<input name="submit" class="btn form-control btn-primary btn-primary submitBtn" type="submit" id="add-committee" value="Add Committee">

												</div>
											</div>
											<div class="col-md-12">
												<span id="showsuccess" class="text-success"> </span>
											</div>
										</div>
									</form>

								</div>
							</div>
						</div>

						<div class="col-xl-12 col-lg-12">
							<div class="card shadow mb-4">
								<!-- Card Header - Dropdown -->
								<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
									<h6 class="m-0 font-weight-bold text-primary">Committee</h6>

								</div>
								<!-- Card Body -->
								<div class="card-body">
									<div class="row">
										<div class="col-md-12">
											<table class="table table-dark">
												<thead>
													<tr><th>#</th> <th> Name</th> <th>Committee</th><th>Image</th><th>Action</th></tr>
												</thead>
												<tbody id="committeebody">

												</tbody>
											</table>											
										</div>
									</div>
								</div>
							</div>
						</div>


					</div>

					<!-- Content Row -->

				</div>
				<!-- /.container-fluid -->

			</div>
			<!-- End of Main Content -->
			<?php include('./includes/footer.php') ?>


		</div>
		<!-- End of Content Wrapper -->

	</div>
	<!-- End of Page Wrapper -->

	<?php include('./includes/scripts.php') ?>

	<script src="jsapi/committee.js"></script>


</body>

</html>
