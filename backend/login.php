<?php
include("includes/connection.php");
session_start();

if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form 

  $myusername = mysqli_real_escape_string($connection,$_POST['username']);
  $mypassword = mysqli_real_escape_string($connection,$_POST['password']); 

  $sql = "SELECT id FROM user WHERE username = '$myusername' and password = '$mypassword'";
  $result = mysqli_query($connection,$sql);
  $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
  $active = $row['active'];

  $count = mysqli_num_rows($result);

      // If result matched $myusername and $mypassword, table row must be 1 row

  if($count == 1) {
   session_register("myusername");
   $_SESSION['login_user'] = $myusername;

   header("location: index.php");
 }else {
   $error = "Your Login Name or Password is invalid";
 }
}
?>
<?php include('./includes/head.php') ?>

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">
      <div class="col-md-12">
        <h2 class="text-center text-white mt-4">Rajiv Gandhi College, Karmad Dist. Aurangabad.</h2>
      </div>
      <div class="col-xl-5 col-lg-9 col-md-6">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Admin Login!</h1>
                  </div>
                  <form class="user" action ="" method="post">
                    <div class="form-group">
                      <input type="text" name="username" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address...">
                    </div>
                    <div class="form-group">
                      <input type="password" name="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">
                    </div>

                    <button type="submit" class="btn btn-primary btn-user btn-block">
                      Login
                    </button>

                  </form>

                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <?php include('./includes/scripts.php') ?>
</body>

</html>
