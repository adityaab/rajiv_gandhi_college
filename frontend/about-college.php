<?php $title = "About College" ?>

<?php include('./include/head.php') ?>

<link href="css/owl.carousel.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">

</head>
<body>

 <?php include('./include/header.php') ?>

 <?php include('include/navigation.php') ?>

 <main role="main">


  <div class="container">
    <!-- Example row of columns -->
    <?php //include('./include/slider.php') ?>

    <div class="row">

      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <h2>History of College </h2>
            <hr>
            <p class="text-justify">
              <div class="founder-image-div">

                <img src="images/dongaokar.jpg" alt="" class="float-right ml-2 mb-2 img-thumbnail" width="200px">
                <p class="text-center font-weight-bold">Late Hon. Ex. M.P. Sahebrao Patil Dongaokar <br>
                (Founder President)</p>
                
              </div>
              Bhartiya Sanskruti Sanvardhan Shikshan Prasarak Mandal Aurangabad is established in the year of 1986-87 and Rajiv Gandhi Arts & Commerce Night College started in 1990. The intension behind start college is to provide education for those section who has willing to complete education but not due to work. From beginning, Rajiv Gandhi College help for workers working in Chikalthana & Waluj MIDC to complete higher education. After tenure Rajiv Gandhi College shifted Aurangabad to Karmad Dist. Aurangabad

            </p>
            <p class="text-justify">Today Rajiv Gandhi College one of the reputed and leading educational institute in Karmad, area and today from rural area, thousands of students have been completed their education. Students strength of college increasing year by year today more than two thousand students have taken admission
            </p>

          </div>

        </div>

        
      </div>

      
    </div>


  </div>

</div> <!-- /container -->

</main>

<?php include('./include/footer.php') ?>
<?php include('./include/scripts.php') ?>


</body>
</html>
