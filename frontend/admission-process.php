<?php $title = "Admission Process" ?>

<?php include('./include/head.php') ?>

<link href="css/owl.carousel.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">

</head>
<body>

	<?php include('./include/header.php') ?>

	<?php include('include/navigation.php') ?>

	<main role="main">


		<div class="container">
			<!-- Example row of columns -->
			<?php //include('./include/slider.php') ?>

			<div class="row">

				<div class="col-md-12">
					<div class="card">
						<div class="card-body">
							<h2>Admission Rules:</h2>
							<hr>
							<table class="table table-hover table-active table-striped">
								<tbody><tr>
									<td>

										
										1). Application in the prescribed form must be accompanied by two recent stamp size photographs
									and documents as mentioned in Prospectus.</td>
								</tr>
								<tr><td>2).The programme for admission for the current session will be displayed on the notice board.
									Applicants are advised to keep themselves informed of the various notices that are displayed on
								the Notice Board and website from time to time.</td></tr>
								<tr><td>3).Application will be considered only when it fulfils the following conditions :
									<ol> <li type="a">It is properly and correctly filled and signed by the applicant & applicant's father/guardian.</li>
										<li type="a"> It is received by the college office on or before the last date prescribed.</li>
										<li type="a">It is accompanied by the attested copies of the following documents.
											<ul><li> School/College Leaving Certificate (T.C.).</li>
												<li> Statement of marks of the last examination passed/failed.</li>
												<li> Migration Certificate in case of a person who has passed the last examination from a Board
												other than Maharashtra State Board or a University other than Dr. B.A.M.U. Aurangabad.</li>
												<li>Caste certificate, Validity and Non Creamy Layer.</li></ul></li></ol></td></tr>
												<tr><td>4).Students who pass the qualifying examination from any other Board/University and desire to seek
												admission should submit an eligibility certificate.</td></tr>
												<tr><td>5).Applicants who have a break in education should submit a gap certificate in the form of a court
												affidavit stating the reason for the gap.</td></tr>
												<tr><td>6).Applicants will be required to surrender the original copy of their School/College leaving certificate
												at the time of finalisation of admission.</td></tr>
												<tr><td>7).Admission will be provisional, subject to the approval of the University, and is liable to be cancelled
												at any time if the applicant has given false information, or university enrolment is not given.</td></tr>
												<tr><td>8). Reservation of seats for candidates belonging to backward classes will be according to the
												government regulations and circulars.</td></tr>
												<tr><td>9). All matters related to admission will be handled by the admission committee. Applicants are
												required to contact this committee during working hours as displayed on the notice board.</td></tr>
												<tr><td>10). Duplicate T. C. (Transfer Certificate) will not be entertained.</td></tr>
											</tbody>
										</table>
									</div></div>


									

									
								</div> <!-- /container -->

							</main>

							<?php include('./include/footer.php') ?>
							<?php include('./include/scripts.php') ?> 
						</body>
						</html>
