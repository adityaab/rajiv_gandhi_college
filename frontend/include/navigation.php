  <nav class="navbar navbar-expand-md navbar-light bg-light" >
    <div class="container">
      <!-- <a class="navbar-brand" href="#">Navbar</a> -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About Us</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="about-college.php">About College</a>
              <a class="dropdown-item" href="principal-message.php">Principal's Message</a>
              <!-- <a class="dropdown-item" href="#">About Discipline</a> -->
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="admission-process.php">Admission Process</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Courses</a>
            <div class="dropdown-menu" aria-labelledby="dropdown02">
              <a class="dropdown-item" href="courses_junior.php">Junior</a>
              <a class="dropdown-item" href="courses_graduation.php">Graduation</a>
              <!-- <a class="dropdown-item" href="#">About Discipline</a> --> 
            </div>
          </li>
          <li class="nav-item"> 
            <a class="nav-link" href="faculty_detail.php">Faculty / Staff</a>
          </li>
          <li class="nav-item"> 
            <a class="nav-link" href="maincommittee.php">Committee</a>
          </li>

          <!-- <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Committee</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="maincommittee.php">Main Committee</a>
              <a class="dropdown-item" href="sportcommittee.php">Sports Committee</a>
              <a class="dropdown-item" href="librarycommittee.php">Library Committee</a>
              <a class="dropdown-item" href="culturalcommittee.php">Cultural Committee</a>
              <a class="dropdown-item" href="disciplinecommiti.php">Discipline Committee</a>
              <a class="dropdown-item" href="student_council.php">Student Council Committee</a>
              <a class="dropdown-item" href="educational_tour.php">Educational Tour Committee</a>
            </div>
          </li> -->  
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown03" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Facilities</a>
            <div class="dropdown-menu" aria-labelledby="dropdown03">
              <a class="dropdown-item" href="scholarship.php">Scholarship</a>
              <a class="dropdown-item" href="competitive.php">Competitive Exam</a>
              <!-- <a class="dropdown-item" href="#">About Discipline</a> --> 
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="fees-structure.php">Fees Structure</a>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Gallery</a>
            <div class="dropdown-menu" id="gallery-dropdown" aria-labelledby="dropdown05">

              <!-- <a class="dropdown-item" href="#">About Discipline</a> --> 
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contact.php">Contact Us</a>
          </li>

        </ul>
      </div>
    </div>
  </nav>

