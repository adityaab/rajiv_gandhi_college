<div class="row">
  <div class="col-md-6">

    <h5 class="mt-3">Junior Arts ( XI<sup>th</sup> )</h5>

    <table class="table table-hover table-active">
      <thead>
        <tr>

          <th>Structure</th>    
          <th>Full fees</th>
          <th>GOI</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Admission Fees</td>
          <td> <i class="fa fa-rupee"></i> &nbsp;16.00</td>
          <td> NA</td>
        </tr>
        <tr>
          <td>Tuition Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;192.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Semester Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;32.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Student Asst Fund</td>
          <td><i class="fa fa-rupee"></i> &nbsp;20.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;20.00</td>
        </tr>
        <tr>
          <td>Identity Card</td>
          <td><i class="fa fa-rupee"></i> &nbsp;20.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;20.00</td>
        </tr>
        <tr>
          <td>Practise Exam Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;100.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Marks Memo Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;10.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;10.00</td>
        </tr>
        <tr>
          <td>Gathering Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Magazine Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;75.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;75.00</td>
        </tr>
        <tr>
          <td>Sports Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;25.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Environmental Education</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Library Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>College Fund</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
        </tr>
        <tr class="font-weight-bold table-secondary">
          <td>Total Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;690.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;175.00</td>
        </tr>
        <tr class="font-weight-bold table-secondary">
          <td>EBC</td>
          <td><i class="fa fa-rupee"></i> &nbsp;482.00</td>
          <td>NA</td>
        </tr>
      </tbody>
    </table>

  </div>
  <div class="col-md-6">
    <h5 class="mt-3">Junior Arts ( XII<sup>th</sup> )</h5>

    <table class="table table-hover table-active">
      <thead>
        <tr>

          <th>Structure</th>    
          <th>Full fees</th>
          <th>GOI</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Admission Fees</td>
          <td> <i class="fa fa-rupee"></i> &nbsp;18.00</td>
          <td> NA</td>
        </tr>
        <tr>
          <td>Tuition Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;216.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Semester Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;36.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Student Asst Fund</td>
          <td><i class="fa fa-rupee"></i> &nbsp;20.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;20.00</td>
        </tr>
        <tr>
          <td>Identity Card</td>
          <td><i class="fa fa-rupee"></i> &nbsp;20.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;20.00</td>
        </tr>
        <tr>
          <td>Practise Exam Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;100.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Marks Memo Fees</td>
          <td>NA</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Gathering Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Magazine Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;75.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;75.00</td>
        </tr>
        <tr>
          <td>Sports Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;25.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Environmental Education</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Library Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>College Fund</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
        </tr>
        <tr class="font-weight-bold table-secondary">
          <td>Total Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;710.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;165.00</td>
        </tr>
        <tr class="font-weight-bold table-secondary">
          <td>EBC</td>
          <td><i class="fa fa-rupee"></i> &nbsp;482.00</td>
          <td>NA</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>