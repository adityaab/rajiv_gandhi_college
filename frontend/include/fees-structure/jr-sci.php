<div class="row">
  <div class="col-md-6">

    <h5 class="mt-3">Junior Science ( XI<sup>th</sup> )</h5>

    <table class="table table-hover table-active">
      <thead>
        <tr>

          <th>Structure</th>    
          <th>Fees</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Admission Fees</td>
          <td> <i class="fa fa-rupee"></i> &nbsp;20.00</td>
        </tr>
        <tr>
          <td>Tuition Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;1000.00</td>
        </tr>
        <tr>
          <td>Semester Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;60.00</td>
        </tr>
        <tr>
          <td>Student Asst Fund</td>
          <td><i class="fa fa-rupee"></i> &nbsp;20.00</td>
        </tr>
        <tr>
          <td>Identity Card</td>
          <td><i class="fa fa-rupee"></i> &nbsp;20.00</td>
        </tr>
        <tr>
          <td>Practise Exam Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;100.00</td>
        </tr>
        <tr>
          <td>Marks Memo Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;10.00</td>
        </tr>
        <tr>
          <td>Gathering Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
        </tr>
        <tr>
          <td>Magazine Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;75.00</td>
        </tr>
        <tr>
          <td>Sports Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
        </tr>
        <tr>
          <td>Environmental Education</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
        </tr>
        <tr>
          <td>Library Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
        </tr>
        <tr>
          <td>College Fund</td>
          <td><i class="fa fa-rupee"></i> &nbsp;150.00</td>
        </tr>
        <tr> 
          <td>Science Journal Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;150.00</td>
        </tr>
        <tr> 
          <td>Laboratory Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;100.00</td>
        </tr>
        <tr>
          <td>Library Development Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;20.00</td>
        </tr>
        <tr>
          <td>Social Activity Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
        </tr>
        <tr class="font-weight-bold table-secondary">
          <td>Total Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;1975.00</td>
        </tr>

      </tbody>
    </table>

  </div>
  <div class="col-md-6">
    <h5 class="mt-3">Junior Science ( XII<sup>th</sup> )</h5>

    <table class="table table-hover table-active">
      <thead>
        <tr>

          <th>Structure</th>    
          <th>Fees</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Admission Fees</td>
          <td> <i class="fa fa-rupee"></i> &nbsp;20.00</td>
        </tr>
        <tr>
          <td>Tuition Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;2500.00</td>
        </tr>
        <tr>
          <td>Semester Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;60.00</td>
        </tr>
        <tr>
          <td>Student Asst Fund</td>
          <td><i class="fa fa-rupee"></i> &nbsp;20.00</td>
        </tr>
        <tr>
          <td>Identity Card</td>
          <td><i class="fa fa-rupee"></i> &nbsp;20.00</td>
        </tr>
        <tr>
          <td>Practise Exam Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;100.00</td>
        </tr>
        <tr>
          <td>Marks Memo Fees</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Gathering Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
        </tr>
        <tr>
          <td>Magazine Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;75.00</td>
        </tr>
        <tr>
          <td>Sports Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
        </tr>
        <tr>
          <td>Environmental Education</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
        </tr>
        <tr>
          <td>Library Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
        </tr>
        <tr>
          <td>College Fund</td>
          <td><i class="fa fa-rupee"></i> &nbsp;150.00</td>
        </tr>
        <tr> 
          <td>Science Journal Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;150.00</td>
        </tr>
        <tr> 
          <td>Laboratory Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;150.00</td>
        </tr>
        <tr>
          <td>Library Development Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;20.00</td>
        </tr>
        <tr>
          <td>Social Activity Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
        </tr>
        <tr class="font-weight-bold table-secondary">
          <td>Total Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;3515.00</td>
        </tr>

      </tbody>
    </table>
  </div>
</div>