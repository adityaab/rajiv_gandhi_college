<div class="row">
  <div class="col-md-12">

    <h5 class="mt-3">B.Com</h5>

    <table class="table table-hover table-active">
      <thead>
        <tr>

          <th>Structure</th>    
          <th>Full Fees</th>
          <th>EBC</th>
          <th>GOI</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Registration Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;25.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;25.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Admission Fees</td>
          <td> <i class="fa fa-rupee"></i> &nbsp;25.00</td>
          <td> <i class="fa fa-rupee"></i> &nbsp;25.00</td>
          <td> NA </td>          
        </tr>
        <tr>
          <td>Tuition Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;25.00</td>
          <td> NA </td>          
          <td> NA </td>                    
        </tr>
        <tr>
          <td>Non Grand</td>
          <td><i class="fa fa-rupee"></i> &nbsp;1600.00</td>
          <td> NA </td>          
          <td> NA </td>                    
        </tr>
        <tr>
          <td>Library Fees</td>
          <td>NA</td>
          <td><i class="fa fa-rupee"></i> &nbsp;40.00</td>
          <td>NA</td> 
        </tr>
        <tr>
          <td>Eligibility Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
          <td>NA</td>
          <td>NA</td> 
        </tr>
        <tr>
          <td>Student Counsil</td>
          <td>NA</td>
          <td><i class="fa fa-rupee"></i> &nbsp;10.00</td>
          <td>NA</td> 
        </tr>
        <tr>
          <td>Gathering Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;10.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Magazine Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Gymkhana & Sports</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Medical Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;15.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Identity Card</td>
          <td><i class="fa fa-rupee"></i> &nbsp;15.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;10.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;10.00</td>
        </tr>
        <tr>
          <td>SAF</td>
          <td><i class="fa fa-rupee"></i> &nbsp;10.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;10.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>University Welfare</td>
          <td><i class="fa fa-rupee"></i> &nbsp;10.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;20.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>College Exam</td>
          <td><i class="fa fa-rupee"></i> &nbsp;20.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;75.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Ashwamedh</td>
          <td><i class="fa fa-rupee"></i> &nbsp;75.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;20.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>University E Suvidha</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Environmental Sci. (II year)</td>
          <td><i class="fa fa-rupee"></i> &nbsp;100.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;100.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Computer Sci. (I year)</td>
          <td>NA</td>
          <td>NA</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>ELE</td>
          <td><i class="fa fa-rupee"></i> &nbsp;10.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;10.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Laboratory Fees</td>
          <td><i class="fa fa-rupee"></i> &nbsp;300.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;300.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>NSS</td>
          <td><i class="fa fa-rupee"></i> &nbsp;10.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;10.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Disaster Management</td>
          <td><i class="fa fa-rupee"></i> &nbsp;20.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;20.00</td>
          <td>NA</td>
        </tr>
        <tr>
          <td>Cultural Activity</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
          <td><i class="fa fa-rupee"></i> &nbsp;50.00</td>
          <td>NA</td>
        </tr> 
      </tbody>
    </table>

  </div>

</div>