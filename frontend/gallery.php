<?php $title = "Gallery" ?>

<?php include('./include/head.php') ?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">

</head>
<body>

	<?php include('./include/header.php') ?>

	<?php include('include/navigation.php') ?>

	<main role="main">


		<div class="container">


			<div class="row">

				<div class="col-md-12">
					<div class="card">
						<div class="card-body">
							<h2 id="eventname">Gallery <span></span></h2>
							<hr>


							<div class="row" id="gallery-row">
							</div> 


						</div>
					</div>


				</div>
			</div>


		</div> <!-- /container -->

	</main>

	<?php include('./include/footer.php') ?>
	<?php include('./include/scripts.php') ?> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
	<script>
		jQuery(document).ready(function($) {
			const queryString = window.location.search;
			const urlParams = new URLSearchParams(queryString);
			const eventname = urlParams.get('eventname');
			$("#eventname span").html(eventname)
			var imageurl="http://localhost:8080/rgc/backend/uploads/";

			getPhotos(eventname);
			function getPhotos(eventname){
				var eventname = eventname;

				$.ajax({
					url:url+'get-event-photos.php',
					method:'GET',
					data:{'eventname':eventname},
					success:function(data){

						var html = "";
						
						for(i=0; i<data.length; i++)
						{
							
							if(data[i].status=='YES'){
								html+="<a href='"+imageurl+data[i].image_path+"' data-toggle='lightbox' data-gallery='gallery' class='col-md-3' title='"+data[i].description+"'>";
								html+="<img src='"+imageurl+data[i].image_path+"' class='img-fluid rounded mb-2' style='height:250px !important'>";
								// html+="<span class='d-block'>"+data[i].description+"</span>";
								html+="</a>";


							} 
						}	

						$('#gallery-row').html(html)

					}
				})	
			}




		});

		$(document).on("click", '[data-toggle="lightbox"]', function(event) {
			event.preventDefault();
			$(this).ekkoLightbox();
		});
	</script>
</body>
</html>
