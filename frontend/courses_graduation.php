<?php $title = "Courses - Graduation Courses" ?>
<?php include('./include/head.php') ?>


</head>
<body>

 <?php include('./include/header.php') ?>

 <?php include('include/navigation.php') ?>

 <main role="main">


  <div class="container">

    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <h2>Graduation level course</h2>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Sr. No.</th>
                  <th scope="col">B.A. Ist, IInd, IIIrd</th>
                  <th scope="col">B.Sc. Ist, IInd, IIIrd</th>

                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td><b>Compulsory Subjects</b></td>
                  <td><b>Compulsory Subjects</b></td>

                </tr>
                <tr>
                  <th scope="row"></th>
                  <td>1. English</td>
                  <td>1. English</td>

                </tr>
                <tr>
                  <th scope="row"></th>
                  <td>2. Marathi/Hindi</td>
                  <td>2. Marathi/Hindi<br>(Second language is for only I & II year )
                  </td>

                </tr>
                <tr>
                  <th scope="row">2</th>
                  <td><b>Objective Subjects</b></td>
                  <td><b>Objective Subjects</b><br>(Students can select any one group from following)</td>
                </tr>
                <tr>
                  <th scope="row"></th>
                  <td><b>Group A :</b> Marathi, Hindi & English</td>
                  <td>1. Physics, Electronics, Computer Science</td>

                </tr>
                <tr>
                  <th scope="row"></th>
                  <td><b>Group B :</b> Psychology, Geography, Library Science, Home Science, Physical Education</td>
                  <td> 2. Physics, Chemistry, Computer Science</td>

                </tr>
                <tr>
                  <th scope="row"></th>
                  <td><b>Group C :</b> Political Science, Sociology</td>
                  <td>3. Physics, Chemistry, Electronics</td>

                </tr>
                <tr>
                  <th scope="row"></th>
                  <td><b>Group D :</b> History</td>
                  <td>4. Physics, Chemistry, Mathematics</td>

                </tr>
                <tr>
                  <th scope="row"></th>
                  <td><b>Group E :</b> Economics, Public
                  Administration</td>
                  <td>5. Physics, Mathematics, Electronics</td>

                </tr>
                <tr>
                  <th scope="row"></th>
                  <td> </td>
                  <td>6. Chemistry, Botany, Zoology</td>

                </tr>
                <tr>
                  <th scope="row"></th>
                  <td> </td>
                  <td>7. Chemistry, Botany, Microbiology</td>

                </tr>
                <tr>
                  <th scope="row"></th>
                  <td> </td>
                  <td>8. Chemistry, Zoology, Microbiology</td>

                </tr>
              </tbody>
            </table> 


          </div>

        </div> <!-- /container -->

      </main>

      <?php include('./include/footer.php') ?>
      <?php include('./include/scripts.php') ?>


    </body>
    </html>
