<?php $title = "fees-structure" ?>

<?php include('./include/head.php') ?>

<link href="css/owl.carousel.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">

</head>
<body>

 <?php include('./include/header.php') ?>

 <?php include('include/navigation.php') ?>

 <main role="main">


  <div class="container">
    <!-- Example row of columns -->
    <?php //include('./include/slider.php') ?>

    <div class="row">

      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <h2>Fees Structure</h2>
            <hr>
            
            <nav>
              <div class="nav nav-tabs nav-pills nav-fill" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-jarts" role="tab" aria-controls="nav-home" aria-selected="true">Jr. Arts</a>
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-jsci" role="tab" aria-controls="nav-profile" aria-selected="false">Jr. Science</a>
                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-ba" role="tab" aria-controls="nav-contact" aria-selected="false">B.A.</a>

                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-bcom" role="tab" aria-controls="nav-contact" aria-selected="false">B.Com</a>
                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-bsc" role="tab" aria-controls="nav-contact" aria-selected="false">B.Sc</a>
              </div>

            </nav>
            <div class="tab-content" id="nav-tabContent">
              <div class="tab-pane fade show active" id="nav-jarts" role="tabpanel" aria-labelledby="nav-jarts-tab">

                <?php include('include/fees-structure/jr-arts.php') ?>

              </div>
              <div class="tab-pane fade" id="nav-jsci" role="tabpanel" aria-labelledby="nav-jsci-tab">
                <?php include('include/fees-structure/jr-sci.php') ?>
              </div>
              <div class="tab-pane fade" id="nav-ba" role="tabpanel" aria-labelledby="nav-ba-tab">
                <?php include('./include/fees-structure/ba.php') ?>
              </div>
              <div class="tab-pane fade" id="nav-bcom" role="tabpanel" aria-labelledby="nav-bcom-tab">
                <?php include('./include/fees-structure/bcom.php')?>
              </div>
              <div class="tab-pane fade" id="nav-bsc" role="tabpanel" aria-labelledby="nav-bsc-tab">
                <?php include('./include/fees-structure/bsc.php') ?>
              </div>

            </div>



          </div>

        </div>


      </div>


    </div>


  </div>

</div> <!-- /container -->

</main>

<?php include('./include/footer.php') ?>
<?php include('./include/scripts.php') ?> 
</body>
</html>
