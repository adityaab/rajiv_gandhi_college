<?php $title = "Welcome to Rajiv Gandhi College" ?>
<?php include('./include/head.php') ?>

<link href="css/owl.carousel.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">

</head>
<body>

 <?php include('./include/header.php') ?>

 <?php include('include/navigation.php') ?>

 <main role="main">


  <div class="container">
    <!-- Example row of columns -->
    <?php include('./include/slider.php') ?>
    
    <?php //include('./include/about.php') ?>

    
    <?php //include('./include/about1.php') ?>

    <?php //include('./include/principal.php') ?>
    <div class="row">

      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
           <h2>Welcome To Rajiv Gandhi College, Karmad </h2>
           <hr>
           <p class="text-justify">
            Our college is one of the most reputed colleges in karmad and area offering quality of education through wide range of UG courses. The urge to render progressive, liberal and relevant education for all sections of the society made them offer Jr. College and UG multiple programmes of varied disciplines. The educational design, methodology and infrastructure here certainly provides for a stimulating environment for teaching, learning and research.
          </p>
          <p class="text-justify">The students are provided with a professional environment, where they not only enrich their academic knowledge but also get involved in promoting diverse skills, character building and social responsibility. Their growth is constantly monitored by well trained and experienced teachers. The college promotes active engagement of students in various associations like N.S.S. and ensures holistic development to them. Workshops, seminars, industrial visits, educational trips, social projects, Inter- collegiate events etc; are periodically organized to help the students have better exposure and emerge out as successful leaders.
          </p>
          <p class="text-justify">
            The journey of Rajiv Gandhi College from a humble beginning to an established educational institution having a constant zeal for development and good work is widely noticed and appreciated. The College will continue to impart education that is secular, gender neutral and sensitive of social needs.
          </p>
        </div>

      </div>

      
    </div>

    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <div class="row">

           <div class="col-md-6">
            <h3 class="text-center">
              <i class="fa fa-eye"></i>
              Our Vision
            </h3>

            <p class="text-justify">
              is to provide strength to the arms and mind of this Indian with the transforming force of education, to create a scientific temper which kills fear and quickens curiosity. We are alert to the double-edged nature of youthful energy, and are committed to harness their boundless enthusiasm and creativity for the mammoth task of nation building.
            </p>

          </div>
          <div class="col-md-6">
            <h3 class="text-center">
              <i class="fa fa-bullseye"></i>

              Our Mission
            </h3>
            <p class="text-justify">
              Our Vision is to make available quality of education for rural and backward area.


            </p>
          </div>
        </div>

      </div>
    </div>
  </div>


</div>

</div> <!-- /container -->

</main>

<?php include('./include/footer.php') ?>
<?php include('./include/scripts.php') ?>

<script src="js/owl.carousel.min.js"></script>
<script>
  $(document).ready(()=>{
    $('.owl-carousel').owlCarousel({
      items:1,
      loop:true,
                // nav:true,
                margin:10,
                autoplay:true,
                autoplayTimeout:3000,
                autoplayHoverPause:true
              });
  })
</script>
</body>
</html>
