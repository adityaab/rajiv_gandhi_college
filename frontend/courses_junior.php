<?php $title = "Courses - Junior Cources" ?>
<?php include('./include/head.php') ?>

</head>
<body>

 <?php include('./include/header.php') ?>

 <?php include('include/navigation.php') ?>

 <main role="main">


  <div class="container">
    

    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <h2>Junior level course </h2>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">SR.NO.</th>
                  <th scope="col">Std. XI and Std. XII SCIENCE</th>
                  <th scope="col">Std. XI and Std. XII ARTS</th>

                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td> Physical Education & Health</td>
                  <td> Physical Education & Health</td>

                </tr>
                <tr>
                  <th scope="row">2</th>
                  <td>Environmental Science</td>
                  <td>Environmental Science</td>

                </tr>
                <tr>
                  <th scope="row">3</th>
                  <td>English</td>
                  <td>English</td>

                </tr>
                <tr>
                  <th scope="row">4</th>
                  <td>Marathi</td>
                  <td>Marathi</td>
                </tr>
                <tr>
                  <th scope="row">5</th>
                  <td>Hindi</td>
                  <td>Hindi</td>

                </tr>
                <tr>
                  <th scope="row">6</th>
                  <td>Physics</td>
                  <td>Political Science</td>

                </tr>
                <tr>
                  <th scope="row">7</th>
                  <td>Chemistry</td>
                  <td>Sociology</td>

                </tr>
                <tr>
                  <th scope="row">8</th>
                  <td>Biology</td>
                  <td>Economics</td>

                </tr>
                <tr>
                  <th scope="row">9</th>
                  <td>Mathematics or Psychology</td>
                  <td>History</td>

                </tr>
                <tr>
                  <th scope="row">10</th>
                  <td> </td>
                  <td>Library Science</td>

                </tr>
              </tbody>
            </table> 



          </div> <!-- /container -->

        </main>

        <?php include('./include/footer.php') ?>
        <?php include('./include/scripts.php') ?>

      </body>
      </html>
