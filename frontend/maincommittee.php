<?php $title = "committee" ?>

<?php include('./include/head.php') ?>

<link href="css/owl.carousel.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">

</head>
<body>

 <?php include('./include/header.php') ?>

 <?php include('include/navigation.php') ?>

 <main role="main">


  <div class="container"> 

    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <h2 class="mt-2">Committee</h2>
            <table class="table table-hover table-active table-striped">
              <thead>
                <tr>

                  <th>Sr.No</th>    
                  <th>Name Of Committee</th>
                  <th>Member Name</th>
                  <th>Photo</th>
                </tr><hr>
              </thead>
              <tbody id="committeebody">


              </tbody>
            </table>

          </div>
        </div>

      </div>

    </div>
  </div>
</main>

<?php include('./include/footer.php') ?>
<?php include('./include/scripts.php') ?> 

<script>
  var imageurl="http://localhost:8080/rgc/backend/uploads/";

  function getCommittee(){
    $.ajax({
      url:url+'add-committee.php',
      method:'GET',
      success:function(data){

        var html = "";
        var sr=0;
        for(i=0; i<data.length; i++)
        {
          sr++;

          html+= "<tr><td>"+sr+"</td><td>"+data[i].name+"</td><td>"+data[i].committee+"</td><td><img src='"+imageurl+data[i].image_path+"' height='100px' width='100px'/></td> </tr>";

        } 

        $('#committeebody').html(html)
        
      }
    })  
  }

  jQuery(document).ready(function($) {
    getCommittee();
  });
</script>

</body>
</html>
