
<?php $title = "Scholorship" ?>
<?php include('./include/head.php') ?>


</head>
<body>

 <?php include('./include/header.php') ?>

 <?php include('include/navigation.php') ?>

 <main role="main">


  <div class="container">
    <!-- Example row of columns --> 


    <!-------------------Start Here-------------->
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <h2>Scholarship </h2>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">SR.NO.</th>
                  <th scope="col">Name of scholarship</th>
                  <th scope="col">Class</th>
                  <th scope="col">Eligibility</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td>Govt. of India Scholarship (GOI)</td>
                  <td>XI & XII<br>
                  B.A., B.Sc & B.Com</td>
                  <td>1) SC, ST, category student whose parents' Annual income is below Rs. Two Lac.<br>
                  2) VJNT, OBC & SBC category student whose Parents annual income is below Rs. One Lac</td>
                </tr>
                <tr>
                  <th scope="row">2</th>
                  <td>	
                  Govt. of India Free ship</td>
                  <td>XI & XII<br>
                  B.A., B.Sc & B.Com</td>
                  <td>	
                    1) VJNT, OBC & SBC category student whose parents' Annual income is more than Rs. One Lac but less tnan Rs. 450.000<br>
                  2) No upper limit to SC, ST category Student</td>
                </tr>
                <tr>
                  <th scope="row">3</th>
                  <td>	
                  Economical Backward Classes</td>
                  <td>XI & XII<br>
                  B.A., B.Sc & B.Com</td>
                  <td>Annual Income is not more than Rs.One Lac for B.Sc. & not more than Rs.15,000 /- for XI & XII</td>
                </tr>
                <tr>
                  <th scope="row">4</th>
                  <td>	
                  Primary Teachers concession (PTC)</td>
                  <td>XI & XII<br>
                  B.A., B.Sc & B.Com</td>
                  <td>Ward of Primary Teachers</td>
                </tr>
                <tr>
                  <th scope="row">5</th>
                  <td>	
                  Secondary Teachers concession (STC)</td>
                  <td>XI & XII<br>
                  B.A., B.Sc & B.Com</td>
                  <td>Ward of Secondary Teachers
                  </td>
                </tr>
                <tr>
                  <th scope="row">6</th>
                  <td>	
                  Ex Service Men Concession</td>
                  <td>XI & XII<br>
                  B.A., B.Sc & B.Com</td>
                  <td>Ward of Ex. Service Men</td>
                </tr>
                <tr>
                  <th scope="row">7</th>
                  <td>Girls Concession</td>
                  <td>XI & XII<br>
                  B.A., B.Sc & B.Com</td>
                  <td>Only Girl students those who are not eligible for other Scholarship or concession</td>
                </tr>
              </tbody>
            </table>



          </div> <!-- /container -->

        </main>

        <?php include('./include/footer.php') ?>

        <?php include('./include/scripts.php') ?>

        
      </body>
      </html>
