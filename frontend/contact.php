<?php $title = "Contact Us" ?>

<?php include('./include/head.php') ?>

<style>

  .input-container {
    display: -ms-flexbox; /* IE10 */
    display: flex;
    width: 100%;
    margin-bottom: 15px;
  }

  .icon {
    padding: 10px;
    background: #0084c2;
    color: white;
    min-width: 50px;
    text-align: center;
  }

  .input-field {
    width: 100%;
    padding: 10px;
    outline: none;
  }
  .input-field:focus {
    border: 2px solid #0084c2;
  }

  /* Set a style for the submit button */
  .btn {
    background-color:#0084c2;
    color: white;
    padding: 10px 15px;
    border: none;
    cursor: pointer;
    /*width: 20%;*/
    opacity: 0.9;
    margin-top:20px;
    border-radius: 10px;
    /*height:40px;*/
  }

  
</style>



</head>
<body>

 <?php include('./include/header.php') ?>

 <?php include('include/navigation.php') ?>

 <main role="main">
   <div class="container">
    <!-- Example row of columns -->

    <div class="row">

      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
           <h2>Contact Us!</h2>
           <hr>
           <div class="row">
            <div class="col-lg-6">
              <div class="input-container">
                <i class="fa fa-user icon"></i>

                <input class="input-field" type="text" placeholder="Name" name="name" >

              </div>

              <div class="input-container">
                <i class="fa fa-envelope icon"></i>
                <input class="input-field" type="text" placeholder="Email" name="email">
              </div>

              <div class="input-container">
                <i class="fa fa-phone icon"></i>
                <input class="input-field" type="text" placeholder="Phone" name="phone">
              </div>

              <div class="input-container">
                <i class="fa fa-book icon"></i>
                <input class="input-field" type="text" placeholder="Subject" name="subject">
              </div>

              <div class="input-container">
                <i class="fa fa-comment icon"></i>
                <textarea class="input-field" type="textarea" placeholder="Message" name="message"></textarea>
              </div>

              <button class="btn"  name="submit" id="submit">Send Message</button>



            </div>
            <div class="col-lg-6">

              <h5 class="mb-3">Rajiv Gandhi College</h5>

              <h6 class=" text-primary"> <i class="fa fa-map"></i> Address</h6>
              <p>Karmad, Dist :- Aurangabad-431007, 
              ( Maharashtra)</p>

              <h6 class="text-primary"> <i class="fa fa-phone"></i> Contact No.</h6>
              <p>  +91 9423452647</p>

              <h6 class="text-primary"> <i class="fa fa-envelope"></i> Email</h6>
              <p>rgckarmad@gmail.com</p>


            </div>
            <div class="col-md-12">
              <hr>
              <h2> Find us here</h2>
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10819.780439491202!2d75.54288718969586!3d19.86444621988599!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bdbaf37ef47bb9b%3A0xce3d3f61668e48b2!2sRajiv%20Gandhi%20College%20Karmad%2CTq.%26Dist.Aurangabad!5e0!3m2!1sen!2sin!4v1585890335388!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>




</main>


<?php include('./include/footer.php') ?>
<?php include('./include/scripts.php') ?> 
</body>
<script>

 jQuery(document).ready(function($) {

  $("#submit").click(function(event) {
    /* Act on the event */

    var name = $("input[name=name]").val();
    var email = $("input[name=email]").val();
    var contact = $("input[name=phone]").val();
    var subject = $("input[name=subject]").val();
    var message = $("textarea[name=message]").val();

    $.ajax({
      url:url+'get-enquiry.php',
      type:'POST',
      dataType:'json',
      data:{'name':name, 'email':email, 'contact':contact, 'subject':subject, 'message':message},
      success:function(data){
        if(data.status==1){
          alert('Enquiy Submitted Successfully')
          $("input, textarea").val("");
        }
      }
    })
    
  });


});




</script>
</html>
