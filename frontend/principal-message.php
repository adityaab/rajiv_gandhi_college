<?php $title = "Principal's Message" ?>
<?php include('./include/head.php') ?>

<link href="css/owl.carousel.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">

</head>
<body>

 <?php include('./include/header.php') ?>

 <?php include('include/navigation.php') ?>

 <main role="main">


  <div class="container">
    <!-- Example row of columns -->
    <?php //include('./include/slider.php') ?>

    <div class="row">

      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
           <h2>Principal's Message </h2>
           <hr>
           <p class="text-justify">

            Rajiv Gandhi Mahavidyalaya Karmad, affilated to Dr. Babasaheb Ambedkar Marathwada University Aurangabad established for the all round development of rural area,s students belonging to all castes including poor, labourer and farmer's pupils. It is the motive of college to provide qualitative education for students specially girls of workers who are working in Chikalthana, Shendra, Ladgaon and Karmad


          </p>
          <p class="text-justify">With the qualitative education for degree courses, Rajiv Gandhi College also started MPSC Classes as well as Pre-recruitment Police Training Center for the students of rural areas. It is the purpose of college that these students reach and aquire their goals in life.

          </p>

          <div class="row">
           <div class="col-md-4">
            <img src="images/vice-principal.jpg" class="img-thumbnail m-auto d-block" alt="">
            <p class="text-center font-weight-bold">
              Vice Principal <br>
              M. B. Deokar, <br>
              Rajiv Gandhi College, Karmad                
            </p>

          </div>
          <div class="col-md-4 offset-md-4">
            <img src="images/principal.jpg" class="img-thumbnail m-auto d-block" alt="">
            <p class="text-center font-weight-bold">
              Principal <br>
              Dr. Naresh Madhavrao  Dahale, <br>
              Rajiv Gandhi College, Karmad                
            </p>

          </div>
        </div>

      </div>

    </div>


  </div>


</div>


</div>

</div> <!-- /container -->

</main>

<?php include('./include/footer.php') ?>
<?php include('./include/scripts.php') ?> 
</body>
</html>
