<?php $title = "faculty" ?>

<?php include('./include/head.php') ?>

<link href="css/owl.carousel.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">

</head>
<body>

 <?php include('./include/header.php') ?>

 <?php include('include/navigation.php') ?>

 <main role="main">


  <div class="container">
    <!-- Example row of columns -->
    <?php //include('./include/slider.php') ?>

    <div class="row">

      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <h2>Faculty/Staff</h2>
            <hr>
            
            <nav>
              <div class="nav nav-tabs nav-pills nav-fill" id="nav-tab" role="tablist">
                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-srfaculty" role="tab" aria-controls="nav-contact" aria-selected="false">Sr.College Staff</a>

                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-jrfaculty" role="tab" aria-controls="nav-contact" aria-selected="false">Jr.College Teaching Staff</a>
                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-nonteaching" role="tab" aria-controls="nav-contact" aria-selected="false">Non Teaching Staff</a>
              </div>

            </nav>
            <div class="tab-content" id="nav-tabContent">
              <div class="tab-pane fade show active" id="nav-srfaculty" role="tabpanel" aria-labelledby="nav-srfaculty-tab">

                <?php include('include/staff/srfaculty.php') ?>

              </div>
              <div class="tab-pane fade" id="nav-jrfaculty" role="tabpanel" aria-labelledby="nav-jrfaculty-tab">
                <?php include('include/staff/jrfaculty.php') ?>
              </div>
              <div class="tab-pane fade" id="nav-nonteaching" role="tabpanel" aria-labelledby="nav-nonteaching-tab">
                <?php include('./include/staff/nonteaching.php') ?>
              </div>
              
            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</div> <!-- /container -->

</main>

<?php include('./include/footer.php') ?>
<?php include('./include/scripts.php') ?> 

<script>
  jQuery(document).ready(function($) {
    var imageurl="http://localhost:8080/rgc/backend/uploads/";

    function getJuniorStaff(){
      var jrstaff="";
      var sr=1;
      $.ajax({
        url:url+'add-faculty.php',
        method:'GET',
        data:{'type':'jr_clg_staff'},
        dataType:'json',
        success:function(data){

          for(i=0; i<data.length; i++){
            jrstaff+="<tr>";
            jrstaff+="<td>"+sr+"</td>";
            jrstaff+="<td>"+data[i].name+"</td>";
            jrstaff+="<td>"+data[i].designation+"</td>";
            jrstaff+="<td>"+data[i].subject+"</td>";
            jrstaff+="<td><img src='"+imageurl+data[i].image_path+"' height='100px' width='100px'/></td>"                                    
            jrstaff+="</tr>";
          }
          $("#jrfaculty").html(jrstaff);
        }
      })
    }
    function getSeniorStaff(){
      var srstaff="";
      var sr=1;
      $.ajax({
        url:url+'add-faculty.php',
        method:'GET',
        data:{'type':'sr_clg_staff'},
        dataType:'json',
        success:function(data){

          for(i=0; i<data.length; i++){
            srstaff+="<tr>";
            srstaff+="<td>"+sr+"</td>";
            srstaff+="<td>"+data[i].name+"</td>";
            srstaff+="<td>"+data[i].designation+"</td>";
            srstaff+="<td>"+data[i].subject+"</td>";
            srstaff+="<td><img src='"+imageurl+data[i].image_path+"' height='100px' width='100px'/></td>";                          
            srstaff+="</tr>";
          }
          $("#srfaculty").html(srstaff);
        }
      })
    }
    function getNonTeachingStaff(){
     var ntstaff="";
     var sr=1;
     $.ajax({
      url:url+'add-faculty.php',
      method:'GET',
      data:{'type':'non_teaching_staff'},
      dataType:'json',
      success:function(data){          

        for(i=0; i<data.length; i++){
          ntstaff+="<tr>";
          ntstaff+="<td>"+sr+"</td>";
          ntstaff+="<td>"+data[i].name+"</td>";
          ntstaff+="<td>"+data[i].designation+"</td>";
          ntstaff+="<td><img src='"+imageurl+data[i].image_path+"' height='100px' width='100px'/></td>";
          // ntstaff+="<td>"+data[i].subject+"</td>";                                    
          ntstaff+="</tr>";
        }
        $("#nonteaching").html(ntstaff);

      }
    })
   }

   getJuniorStaff();
   getSeniorStaff();
   getNonTeachingStaff();

 });

</script>
<!-- <option value="sr_clg_staff">Senior College Staff</option>
                          <option value="jr_clg_staff">Junior College Staff</option>
                          <option value="non_teaching_staff"> -->
                          </body>
                          </html>
